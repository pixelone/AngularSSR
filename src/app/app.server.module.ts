/**
 * ${FILE_NAME}
 *
 * @copyright   Copyright (C) 2017 Fabien Robert. All rights reserved.
 * @author      Fabien Robert (fabien@pixelone.fr)
 */
import {NgModule} from '@angular/core';
import {ServerModule} from '@angular/platform-server';

import {AppModule} from './dashboard/app.module';
import {AppComponent} from './dashboard/app.component';

@NgModule({
  imports: [
    // The AppServerModule should import your AppModule followed
    // by the ServerModule from @angular/platform-server.
    AppModule,
    ServerModule,
  ],
  // Since the bootstrapped component is not inherited from your
  // imported AppModule, it needs to be repeated here.
  bootstrap: [AppComponent],
})
export class AppServerModule {}